import itertools

import ufw.common

__virtualname__ = 'ufw'

def __virtual__():
    if 'ufw.status' in __salt__:
        return __virtualname__
    return False


def enabled(name):
    '''
    Enable the UFW firewall.
    '''

    ret = dict(name=name, changes={}, comment='', result=False)

    status = __salt__['ufw.status']()
    if status['status'] == 'active':
        ret['comment'] = 'UFW already enabled.'
        ret['result'] = True
        return ret

    if __opts__['test']:
        ret['result'] = None
        ret['comment'] = 'UFW will be enabled.'
        return ret

    msg = __salt__['ufw.set_enabled'](enabled=True)
    ret['comment'] = msg

    status = __salt__['ufw.status']()
    if status['status'] == 'active':
        ret['result'] = True

    return ret


def allow(name, **kwargs):
    ret = dict(name=name, changes={}, comment='', result=False)

    # If the ``name`` of the state is a valid UFW profile, use it as the
    # destination app for simplicity's sake
    dapp = None
    try:
        __salt__['ufw.find_application_name'](profile_name=name)
        dapp = name
        kwargs.setdefault('dapp', dapp)
    except ufw.common.UFWError:
        pass

    status_before = __salt__['ufw.status']()

    rule = __salt__['ufw.build_rule'](action='allow', **kwargs)

    ret['comment'] = __salt__['ufw.apply_rule'](rule=rule)

    status_after = __salt__['ufw.status']()
    ret['changes'] = _status_changes(status_before, status_after)
    ret['result'] = True

    return ret


def _status_changes(status_before, status_after):
    changes = {}
    for t1, t2 in itertools.izip_longest(status_before.iteritems(),
                                         status_after.iteritems()):
        key = None
        old = None
        new = None

        if t1:
            key, old = t1
        if t2:
            key, new = t2

        if new != old:
            changes[key] = dict(old=old, new=new)

    return changes

